# Wine Quality
----
Questo repository contiene il progetto di Data Intensive 2019/20.

Scopo: generare modelli di classificazione in grado di prevedere la qualità del vino bianco proveniente dal Portogallo.

Creato da Bazzocchi Luca
(luca.bazzocchi2@studio.unibo.it)

### Link al progetto su Colab
https://colab.research.google.com/drive/1x1_q3Ev5N9ca78C67O3XACfI-lVc8rX_?usp=sharing